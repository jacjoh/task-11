INSERT INTO Characters (Name)
VALUES 
	('RPG');

INSERT INTO Assassins (Type, CharacterID)
VALUES
	('Assassins', 1);

INSERT INTO Warriors (Type, CharacterID)
VALUES
	('Warriors', 1);

INSERT INTO Wizards (Type, CharacterID)
VALUES
	('Wizards', 1);

INSERT INTO Dwarf (Name, HP, Mana, ArmorRating, WarriorID)
VALUES
	('Gimli', 50, 50, 50, 1);

INSERT INTO	Knight (Name, HP, Mana, ArmorRating, WarriorID)
VALUES
	('Lancelot', 50, 50, 50, 1);

INSERT INTO	Summoner (Name, HP, Mana, ArmorRating, WizardID)
VALUES
	('Lysander', 50, 50, 50, 1);

INSERT INTO	Priest (Name, HP, Mana, ArmorRating, WizardID)
VALUES
	('Crocker', 50, 50, 50, 1);

INSERT INTO	Ninja (Name, HP, Mana, ArmorRating, AssassinID)
VALUES
	('Sasuke', 50, 50, 50, 1);

INSERT INTO	Monk (Name, HP, Mana, ArmorRating, AssassinID)
VALUES
	('James', 50, 50, 50, 1);

