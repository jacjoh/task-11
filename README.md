# task-11

RPG generator.

Windows forms application where you can create RPG characters, and see the stats of the characters created.

# Get started
To start you can run the DataBaseScript.sql in Microsoft SQL Server Management Studio to download the database used. Then run InsertDataQuery to add some data to the database. This is needed to run the windows forms program. To run the programs you just opens the file and then execute it. 

The windows forms application is just to run in visual studio. 

# The program
In the program you can add new characters. check existing characters, update the characters, and delete characters from the list.
 