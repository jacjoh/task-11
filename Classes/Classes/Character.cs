﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Classes
{
    public abstract class Characters
    {
        public int ID { get; set; }
        public string Type { get; set; }
        public string Name { get; set; }
        public int HP { get; set; }
        public int Mana { get; set; }
        public int ArmorRating { get; set; }

        public abstract void Attack();
        public abstract void Move();
    }
}
