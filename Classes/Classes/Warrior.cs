﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Classes
{
    public abstract class Warriors: Characters
    {
    }

    public class Knight : Warriors
    {
        public Knight(string name, int hp, int mana, int armorRating, int id)
        {
            ID = id;
            Name = name;
            HP = hp;
            Mana = mana;
            ArmorRating = armorRating;
            Type = "Warrior";
        }
        public override void Attack()
        {
            Console.WriteLine("Swings the sword");
        }

        public override void Move()
        {
            Console.WriteLine("Runs");
        }
    }
    public class Dwarf : Warriors
    {
        public Dwarf(string name, int hp, int mana, int armorRating, int id)
        {
            ID = id;
            Name = name;
            HP = hp;
            Mana = mana;
            ArmorRating = armorRating;
            Type = "Warrior";
        }
        public override void Attack()
        {
            Console.WriteLine("Swings the axe");
        }

        public override void Move()
        {
            Console.WriteLine("Runs");
        }
    }

}
