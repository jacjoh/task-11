﻿using Classes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Text;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;


namespace RPGGenerator
{
    public partial class Form1 : Form
    {
        //Holds data for a new character
        struct CharacterToAdd
        {
            public string type;
            public string name;
            public int hp;
            public int mana;
            public int armor;
        }

        //Stores the id of a chosen character
        private int UpdateID;

        private CharacterToAdd characterToAdd = new CharacterToAdd();

        //The different sub-types listed in comboboxes
        private string[] subtypes = {
                "Priest",
                "Summoner",
                "Knight",
                "Dwarf",
                "Monk",
                "Ninja"
            };

        public Form1()
        {
            InitializeComponent();
            
            //Adds data to the first combobox
            InitializeInputComponents();
        }

        //Removes data from the input components
        private void ClearComponents()
        {
            tbName.Text = "";
            nupHp.Value = 0;
            nupMana.Value = 0;
            nupArmor.Value = 0;
        }

        private void InitializeInputComponents()
        {
            lbCharacters.Items.Clear();
            cbTypes.Items.AddRange(subtypes);
            cbCharacters.Items.AddRange(subtypes);

            ClearComponents();
        }

        //checks if all the parameters are added 
        private bool CheckForParameters()
        {
            if(characterToAdd.name == "" || characterToAdd.name == null)
            {
                MessageBox.Show("Must add a name to the character");
                return false;
            }
            else if (!CheckIfTypeIsDefined())
            {
                MessageBox.Show("Must specify the type of character");
                return false;
            }
            else if (characterToAdd.hp > 100 || characterToAdd.hp < 1)
            {
                MessageBox.Show("The HP must be defined between 0 and 100");
                return false;
            }
            else if (characterToAdd.mana > 100 || characterToAdd.mana < 1)
            {
                MessageBox.Show("The Mana must be defined between 0 and 100");
                return false;
            }
            else if (characterToAdd.armor > 100 || characterToAdd.armor < 1)
            {
                MessageBox.Show("The Armor Rating must be defined between 0 and 100");
                return false;
            }
            else
            {
                return true;
            }
        }

        private bool CheckIfTypeIsDefined()
        {
            if (characterToAdd.type == null)
                return false;

            foreach(string charType in subtypes)
            {
                if (charType == characterToAdd.type)
                    return true;
            }
            return false;
        }

        private void btn_Click(object sender, EventArgs e)
        {
            //Adds a character to the db if all parameters are defined
            if (CheckForParameters() && UpdateID == 0)
            {
                Characters character = AddCharacter();
                MessageBox.Show("Created Character");
                ClearComponents();
            }
            else if(UpdateID != 0)
            {
                MessageBox.Show("To create a new character you must press create new. \n" +
                    "To update the character selected you can presss update");
            }
        }

        private void UpdateValuesInListBox(Characters newcharacter)
        {
            if(newcharacter.GetType().Name == cbTypes.SelectedItem.ToString())
            {
                //Update lb
                lbCharacters.Items.Clear();
                List<Characters> characters = DBHandler.SelectAllCharactersFromDatabase(cbTypes.SelectedItem.ToString());
                foreach (Characters character in characters)
                {
                    lbCharacters.Items.Add(character.ID + "\t" + character.Name);
                }
            }
        }

        private void cbCharacters_SelectedIndexChanged(object sender, EventArgs e)
        {
            characterToAdd.type = cbCharacters.SelectedItem.ToString();
        }

        private void tbName_TextChanged(object sender, EventArgs e)
        {
            characterToAdd.name = tbName.Text;
        }

        private void nupHp_ValueChanged(object sender, EventArgs e)
        {
            characterToAdd.hp = (int)nupHp.Value;
        }

        private void nupMana_ValueChanged(object sender, EventArgs e)
        {
            characterToAdd.mana = (int)nupMana.Value;
        }

        private void nupArmor_ValueChanged(object sender, EventArgs e)
        {
            characterToAdd.armor = (int)nupArmor.Value;
        }
       
        //Creates a character object and adds it to the db
        private Characters AddCharacter()
        {
            Characters character = DBHandler.CreateCharacterObject(characterToAdd.type, characterToAdd.name,
                characterToAdd.hp, characterToAdd.mana, characterToAdd.armor, 1);
            DBHandler.InsertToDatabase(character);
            MessageBox.Show($"- Type: {character.Type} \n- Name: {character.Name}" +
                        $"\n- HP: {character.HP} \n- Mana: {character.Mana} \n- Armor rating: {character.ArmorRating}");
            return character;
        }

        private void cbType_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Clears the list box and then collects all the characters for the specified type from the db
            lbCharacters.Items.Clear();
            List<Characters> characters = DBHandler.SelectAllCharactersFromDatabase(cbTypes.SelectedItem.ToString());
            foreach(Characters character in characters)
            {
                lbCharacters.Items.Add(character.ID + "\t" + character.Name);
            }
        }

        //Fills the input fields with data from a selected character
        private void FillComponentsWithData(Characters character)
        {
            tbName.Text = character.Name;
            nupHp.Value = character.HP;
            nupMana.Value = character.Mana;
            nupArmor.Value = character.ArmorRating;
            cbCharacters.SelectedItem = character.GetType().Name;
        }

        private void lbCharacters_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Must chexk if there are selected any character
            if(lbCharacters.SelectedItem.ToString().Length != 0)
            {
                Characters character = DBHandler.SelectOneCharacterFromDatabase(cbTypes.SelectedItem.ToString(), lbCharacters.SelectedItem.ToString());
                FillComponentsWithData(character);
                //Sets the id for the selected character to make it easier to update it
                UpdateID = Int32.Parse(lbCharacters.SelectedItem.ToString().Split('\t')[0]);
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            //If there are no selected character, one cannot update
            if(UpdateID == 0)
            {
                MessageBox.Show("Must Choose a character to update");
                return;
            }
            Characters character = DBHandler.CreateCharacterObject(characterToAdd.type, characterToAdd.name,
                characterToAdd.hp, characterToAdd.mana, characterToAdd.armor, 1);
            DBHandler.UpdateInDatabase(character, UpdateID);
        }
        
        //Delete button
        private void button1_Click(object sender, EventArgs e)
        {
            DBHandler.DeleteInDatabase(cbTypes.SelectedItem.ToString(), UpdateID);
        }

        //Create new character button
        private void button2_Click(object sender, EventArgs e)
        {
            ClearComponents();
            UpdateID = 0;
        }
    }
}
