﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Classes
{
    public abstract class Assassins:Characters
    {
    }

    public class Monk : Assassins
    {
        public Monk(string name, int hp, int mana, int armorRating, int id)
        {
            ID = id;
            Name = name;
            HP = hp;
            Mana = mana;
            ArmorRating = armorRating;
            Type = "Assassin";
        }
        public override void Attack()
        {
            Console.WriteLine("Kicks");
        }

        public override void Move()
        {
            Console.WriteLine("Runs fast");
        }
    }

    public class Ninja : Assassins
    {
        public Ninja(string name, int hp, int mana, int armorRating, int id)
        {
            ID = id;
            Name = name;
            HP = hp;
            Mana = mana;
            ArmorRating = armorRating;
            Type = "Assassin";
        }
        public override void Attack()
        {
            Console.WriteLine("Stabs");
        }

        public override void Move()
        {
            Console.WriteLine("Runs fast");
        }
    }
}
