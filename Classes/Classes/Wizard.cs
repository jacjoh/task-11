﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Classes
{
    public abstract class Wizards : Characters
    {
    }

    public class Priest : Wizards
    {
        public Priest(string name, int hp, int mana, int armorRating, int id)
        {
            ID = id;
            Name = name;
            HP = hp;
            Mana = mana;
            ArmorRating = armorRating;
            Type = "Wizard";
        }

        public override void Attack()
        {
            Console.WriteLine("Heals");
        }

        public override void Move()
        {
            Console.WriteLine("Walks");
        }

    }

    public class Summoner : Wizards
    {
        public Summoner(string name, int hp, int mana, int armorRating, int id)
        {
            ID = id;
            Name = name;
            HP = hp;
            Mana = mana;
            ArmorRating = armorRating;
            Type = "Wizard";
        }

        public override void Attack()
        {
            Console.WriteLine("Summons a beast");
        }

        public override void Move()
        {
            Console.WriteLine("Walks");
        }

    }
}
