﻿namespace RPGGenerator
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.tbName = new System.Windows.Forms.TextBox();
            this.btn_character = new System.Windows.Forms.Button();
            this.cbCharacters = new System.Windows.Forms.ComboBox();
            this.nupHp = new System.Windows.Forms.NumericUpDown();
            this.nupMana = new System.Windows.Forms.NumericUpDown();
            this.nupArmor = new System.Windows.Forms.NumericUpDown();
            this.btnExistingCharacter = new System.Windows.Forms.Button();
            this.cbTypes = new System.Windows.Forms.ComboBox();
            this.lbCharacters = new System.Windows.Forms.ListBox();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.nupHp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nupMana)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nupArmor)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(160, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Create a RPG character";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 54);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(292, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "What type of character do you want to create";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 103);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(45, 17);
            this.label3.TabIndex = 3;
            this.label3.Text = "Name";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 158);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(27, 17);
            this.label4.TabIndex = 4;
            this.label4.Text = "HP";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(15, 211);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(43, 17);
            this.label5.TabIndex = 5;
            this.label5.Text = "Mana";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(13, 257);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(91, 17);
            this.label6.TabIndex = 6;
            this.label6.Text = "Armor Rating";
            // 
            // tbName
            // 
            this.tbName.Location = new System.Drawing.Point(325, 103);
            this.tbName.Name = "tbName";
            this.tbName.Size = new System.Drawing.Size(120, 22);
            this.tbName.TabIndex = 9;
            this.tbName.TextChanged += new System.EventHandler(this.tbName_TextChanged);
            // 
            // btn_character
            // 
            this.btn_character.Location = new System.Drawing.Point(16, 310);
            this.btn_character.Name = "btn_character";
            this.btn_character.Size = new System.Drawing.Size(118, 47);
            this.btn_character.TabIndex = 13;
            this.btn_character.Text = "Add";
            this.btn_character.UseVisualStyleBackColor = true;
            this.btn_character.Click += new System.EventHandler(this.btn_Click);
            // 
            // cbCharacters
            // 
            this.cbCharacters.FormattingEnabled = true;
            this.cbCharacters.Location = new System.Drawing.Point(324, 54);
            this.cbCharacters.Name = "cbCharacters";
            this.cbCharacters.Size = new System.Drawing.Size(121, 24);
            this.cbCharacters.TabIndex = 14;
            this.cbCharacters.SelectedIndexChanged += new System.EventHandler(this.cbCharacters_SelectedIndexChanged);
            // 
            // nupHp
            // 
            this.nupHp.Location = new System.Drawing.Point(325, 153);
            this.nupHp.Name = "nupHp";
            this.nupHp.Size = new System.Drawing.Size(120, 22);
            this.nupHp.TabIndex = 15;
            this.nupHp.ValueChanged += new System.EventHandler(this.nupHp_ValueChanged);
            // 
            // nupMana
            // 
            this.nupMana.Location = new System.Drawing.Point(324, 206);
            this.nupMana.Name = "nupMana";
            this.nupMana.Size = new System.Drawing.Size(120, 22);
            this.nupMana.TabIndex = 16;
            this.nupMana.ValueChanged += new System.EventHandler(this.nupMana_ValueChanged);
            // 
            // nupArmor
            // 
            this.nupArmor.Location = new System.Drawing.Point(324, 257);
            this.nupArmor.Name = "nupArmor";
            this.nupArmor.Size = new System.Drawing.Size(120, 22);
            this.nupArmor.TabIndex = 17;
            this.nupArmor.ValueChanged += new System.EventHandler(this.nupArmor_ValueChanged);
            // 
            // btnExistingCharacter
            // 
            this.btnExistingCharacter.Location = new System.Drawing.Point(574, 310);
            this.btnExistingCharacter.Name = "btnExistingCharacter";
            this.btnExistingCharacter.Size = new System.Drawing.Size(88, 47);
            this.btnExistingCharacter.TabIndex = 20;
            this.btnExistingCharacter.Text = "See info";
            this.btnExistingCharacter.UseVisualStyleBackColor = true;
            // 
            // cbTypes
            // 
            this.cbTypes.FormattingEnabled = true;
            this.cbTypes.Location = new System.Drawing.Point(574, 54);
            this.cbTypes.Name = "cbTypes";
            this.cbTypes.Size = new System.Drawing.Size(204, 24);
            this.cbTypes.TabIndex = 21;
            this.cbTypes.SelectedIndexChanged += new System.EventHandler(this.cbType_SelectedIndexChanged);
            // 
            // lbCharacters
            // 
            this.lbCharacters.FormattingEnabled = true;
            this.lbCharacters.ItemHeight = 16;
            this.lbCharacters.Location = new System.Drawing.Point(574, 105);
            this.lbCharacters.Name = "lbCharacters";
            this.lbCharacters.Size = new System.Drawing.Size(204, 180);
            this.lbCharacters.TabIndex = 23;
            this.lbCharacters.SelectedIndexChanged += new System.EventHandler(this.lbCharacters_SelectedIndexChanged);
            // 
            // btnUpdate
            // 
            this.btnUpdate.Location = new System.Drawing.Point(175, 310);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(120, 47);
            this.btnUpdate.TabIndex = 24;
            this.btnUpdate.Text = "Update ";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(325, 310);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(119, 47);
            this.btnDelete.TabIndex = 25;
            this.btnDelete.Text = "Delete ";
            this.btnDelete.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(689, 310);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(89, 47);
            this.button1.TabIndex = 26;
            this.button1.Text = "Delete";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(324, 10);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(120, 23);
            this.button2.TabIndex = 27;
            this.button2.Text = "Create new";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(571, 16);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(123, 17);
            this.label7.TabIndex = 28;
            this.label7.Text = "Stored Characters";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.lbCharacters);
            this.Controls.Add(this.cbTypes);
            this.Controls.Add(this.btnExistingCharacter);
            this.Controls.Add(this.nupArmor);
            this.Controls.Add(this.nupMana);
            this.Controls.Add(this.nupHp);
            this.Controls.Add(this.cbCharacters);
            this.Controls.Add(this.btn_character);
            this.Controls.Add(this.tbName);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.nupHp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nupMana)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nupArmor)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tbName;
        private System.Windows.Forms.Button btn_character;
        private System.Windows.Forms.ComboBox cbCharacters;
        private System.Windows.Forms.NumericUpDown nupHp;
        private System.Windows.Forms.NumericUpDown nupMana;
        private System.Windows.Forms.NumericUpDown nupArmor;
        private System.Windows.Forms.Button btnExistingCharacter;
        private System.Windows.Forms.ComboBox cbTypes;
        private System.Windows.Forms.ListBox lbCharacters;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label7;
    }
}

