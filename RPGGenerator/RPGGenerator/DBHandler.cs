﻿using Classes;
using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace RPGGenerator
{
    class DBHandler
    {
        //Adds a character to the db
        public static void InsertToDatabase(Characters character)
        {
            SqlConnection builder = new SqlConnection();
            builder.ConnectionString = "Server=PC7595\\SQLEXPRESS;Database=RPGCharacters;Trusted_Connection=True";

            string sql = $"INSERT INTO {character.GetType().Name} (Name, HP, Mana, ArmorRating, {character.Type}ID) VALUES " +
                $"(@Name, @HP, @Mana, @ArmorRating, @ID)";

            try
            {
                //Connect to db
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();
                    try
                    {
                        //Add the character to the db
                        using (SqlCommand command = new SqlCommand(sql, connection))
                        {
                            command.Parameters.AddWithValue("@Name", character.Name);
                            command.Parameters.AddWithValue("@HP", character.HP);
                            command.Parameters.AddWithValue("@Mana", character.Mana);
                            command.Parameters.AddWithValue("@ArmorRating", character.ArmorRating);
                            command.Parameters.AddWithValue("@ID", 1);

                            command.ExecuteNonQuery();
                        }
                    }
                    catch (Exception ex)
                    {
                        //Problem when executing sql query
                        Console.WriteLine(ex.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                //Problem when connecting to db
                Console.WriteLine(ex.Message);
            }
        }

        //Selects all the characters for a specified subtype
        public static List<Characters> SelectAllCharactersFromDatabase(string tableName)
        {
            SqlConnection builder = new SqlConnection();
            builder.ConnectionString = "Server=PC7595\\SQLEXPRESS;Database=RPGCharacters;Trusted_Connection=True";

            string sql = $"SELECT * " +
                $"FROM {tableName} ";

            List<Characters> characters = new List<Characters>();
            try
            {
                //Connect to db
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();
                    try
                    {
                        //Get all the characters stored in the db
                        using (SqlCommand command = new SqlCommand(sql, connection))
                        {
                            using(SqlDataReader reader = command.ExecuteReader())
                            {
                                //Create objects and add to a list
                                while (reader.Read())
                                {
                                    characters.Add(CreateCharacterObject(tableName, reader.GetString(1), 
                                        reader.GetInt32(2), reader.GetInt32(3), reader.GetInt32(4), reader.GetInt32(0)));
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        //Problem when executing sql query
                        Console.WriteLine(ex.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                //Problem when connecting to db
                Console.WriteLine(ex.Message);
            }
            return characters;
        }

        //Selects a character based on the type and id passed
        public static Characters SelectOneCharacterFromDatabase(string characterType, string characterSelected)
        {
            SqlConnection builder = new SqlConnection();
            builder.ConnectionString = "Server=PC7595\\SQLEXPRESS;Database=RPGCharacters;Trusted_Connection=True";

            //Must have the id to select the right character from db
            int ID = Int32.Parse(characterSelected.Split('\t')[0]);

            string sql = $"SELECT * " +
                $"FROM {characterType} " +
                $"WHERE {characterType}ID = @ID";

            Characters character = null;
            try
            {
                //Connect to db
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();
                    try
                    {
                        //Get all the characters stored in the db
                        using (SqlCommand command = new SqlCommand(sql, connection))
                        {
                            command.Parameters.AddWithValue("@ID", ID);
                            using (SqlDataReader reader = command.ExecuteReader())
                            {
                                //Create objects and add to return
                                while (reader.Read()) 
                                {
                                    character = CreateCharacterObject(characterType, reader.GetString(1),
                                        reader.GetInt32(2), reader.GetInt32(3), reader.GetInt32(4), reader.GetInt32(0));
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        //Problem when executing sql query
                        Console.WriteLine(ex.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                //Problem when connecting to db
                Console.WriteLine(ex.Message);
            }
            return character;
        }

        //creates an character object 
        public static Characters CreateCharacterObject(string type, string name, int hp, int mana, int armorRating, int id)
        {
            switch (type)
            {
                case "Dwarf":
                    return new Dwarf(name, hp, mana, armorRating, id);
                case "Knight":
                    return new Knight(name, hp, mana, armorRating, id);
                case "Priest":
                    return new Priest(name, hp, mana, armorRating, id);
                case "Summoner":
                    return new Summoner(name, hp, mana, armorRating, id);
                case "Ninja":
                    return new Ninja(name, hp, mana, armorRating, id);
                case "Monk":
                    return new Monk(name, hp, mana, armorRating, id);
                default:
                    return null;
            }
        }

        //Updates the values of a character in the db
        public static void UpdateInDatabase(Characters character, int id)
        {
            SqlConnection builder = new SqlConnection();
            builder.ConnectionString = "Server=PC7595\\SQLEXPRESS;Database=RPGCharacters;Trusted_Connection=True";


            string sql = $"UPDATE {character.GetType().Name} SET Name = @Name, HP = @HP, Mana = @Mana, " +
                $"ArmorRating = @ArmorRating WHERE {character.GetType().Name}ID = @ID";
            try
            {
                //Connect to db
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();
                    try
                    {
                        using (SqlCommand command = new SqlCommand(sql, connection))
                        {
                            command.Parameters.AddWithValue("@Name", character.Name);
                            command.Parameters.AddWithValue("@HP", character.HP);
                            command.Parameters.AddWithValue("@Mana", character.Mana);
                            command.Parameters.AddWithValue("@ArmorRating", character.ArmorRating);
                            command.Parameters.AddWithValue("@ID", id);

                            command.ExecuteNonQuery();
                        }
                    }
                    catch (Exception ex)
                    {
                        //Problem when executing sql query
                        Console.WriteLine(ex.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                //Problem when connecting to db
                Console.WriteLine(ex.Message);
            }
        }

        //Deletes a character from the db
        public static void DeleteInDatabase(string type, int id)
        {
            SqlConnection builder = new SqlConnection();
            builder.ConnectionString = "Server=PC7595\\SQLEXPRESS;Database=RPGCharacters;Trusted_Connection=True";

            //Uses id to delete the right character
            string sql = $"DELETE FROM {type} where {type}ID = @ID"; 
            try
            {
                //Connect to db
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();
                    try
                    {
                        using(SqlCommand command = new SqlCommand(sql, connection))
                        {
                            command.Parameters.AddWithValue("@ID", id);
                            command.ExecuteNonQuery();
                        }
                    }
                    catch (Exception ex)
                    {
                        //Problem when executing sql query
                        Console.WriteLine(ex.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                //Problem when connecting to db
                Console.WriteLine(ex.Message);
            }
        }
    }
}
